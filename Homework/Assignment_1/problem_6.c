#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void sum_first(int A[], int n, int* result)
{
  int i, r=0;
  for (i = 0; i<n; i++)
  {
    r = r+A[i];
  }

  *result = r;
}

int main()
{
  int A[] = {17, -3, 8, 49, -5, 56, 100, -22};
  int result1=0, result2=0;
  printf(" Array = {17, -3, 8, 49, -5, 56, 100, -22}\n");
  sum_first(A, 4, &result1);
  printf("sum first 4: %d\n", result1);
  sum_first(A, 6, &result2);
  printf("sum first 6: %d\n", result2);

  int i;
  scanf("%d", &i);
  return 0;
}
