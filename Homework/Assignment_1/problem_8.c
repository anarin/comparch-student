#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int fib(unsigned n)
{
  if (n == 1)
  {
    printf("Memory address of term %1$d: %2$p\n",n, &n);
    return 1;
  }
  else if (n == 2)
  {
    printf("Memory address of term %1$d: %2$p\n",n, &n);
    return 1;
  }
  else
  {
    printf("Memory address of term %1$d: %2$p\n",n, &n);
    return fib(n-1) + fib(n-2);
  }
}

int main()
{
  int j = fib(5);
  printf("5th term of the fib sequence is: %1$d\n", j);
  int i;
  scanf("%d", &i);
  return 0;
}
