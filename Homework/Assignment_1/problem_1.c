//prints the answers to problem one
#include <stdio.h>

int main(){
  int a = -85;
  int b = 18;
  int c = 0xcd;

  //Now print the values;
  printf("hex \t\tunsigned \tsigned\n");
  printf("%1$hhx\t\t%1$hhu\t\t%1$hhd\n",a);
  printf("%1$hhx\t\t%1$hhu\t\t%1$hhd\n",b);
  printf("%1$hhx\t\t%1$hhu\t\t%1$hhd\n",c);

  int i;
  scanf("%d", &i);
  return 0;
}
