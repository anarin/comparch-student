#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int concat(short m, short n)
{
  int c = 0;
  c = m << 16;
  return c | n;
}

int main()
{
  printf("concat(0xabcd, 0x1234): %1$x\n", concat(0xabcd, 0x1234));
  printf("concat(0xabcd, 0x4321): %1$x\n", concat(0xabcd, 0x4321));
  printf("concat(0xffff, 0xab00): %1$x\n", concat(0xffff, 0xab00));
  printf("concat(0x3300, 0x0033): %1$x\n", concat(0x3300, 0x0033));
  int i;
  scanf("%d", &i);
  return 0;
}
