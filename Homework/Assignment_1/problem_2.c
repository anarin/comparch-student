//prints the binary representation of a series of numbers
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void printbin(char n)
{
  printf("%d in binary: ",n);
  int c,k;
  for (c = 7; c >= 0; c--)
  {
    k = n >> c;

    if (k & 1)
      printf("1");
    else
      printf("0");
  }

  printf("\n");
}

int main()
{
  int a = -128, b = 65, c = 5, d = -1, e=0, f=1, g=5, h=65, i=127;
  printbin(a);
  printbin(b);
  printbin(c);
  printbin(d);
  printbin(e);
  printbin(f);
  printbin(g);
  printbin(h);
  printbin(i);

  int k;
  scanf("%d",&k);
  return 0;
}
