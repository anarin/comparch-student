//Returns the bit range of an int

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

unsigned createMask(unsigned lo, unsigned hi)
{
   unsigned i, r = 0;
   for (i=lo; i<=hi; i++)
       r |= 1 << i;

   return r;
}

unsigned bitrange(unsigned inst, unsigned hi, unsigned lo)
{

  unsigned r = createMask(lo, hi);
  r = r >> lo;
  inst = inst >> lo;
  unsigned result = r & inst;

  return result;

}

int main()
{

  unsigned inst = 0xabcd1234;
  printf("inst: %1$08x\n", inst);
  printf("Bitrange 3-0: 0x%1$x\n", bitrange(inst,3,0));
  printf("Bitrange 31-28: 0x%1$x\n", bitrange(inst,31,28));
  printf("Bitrange 27-4: 0x%1$x\n", bitrange(inst,27,4));
  printf("Bitrange 31-20: 0x%1$x\n", bitrange(inst,31,20));
  printf("Bitrange 0-2: 0x%1$x\n", bitrange(inst,2,0));

  int  number;
	scanf("%d", &number);
}
