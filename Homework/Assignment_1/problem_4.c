#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

unsigned createMask(unsigned lead_num,unsigned size)
{
   unsigned i, r = lead_num;
   for (i=32; i>size; i--)
   {
       r = r<<1;
       r += lead_num;
   }
   r = r << size;

   return r;
}

//takes a number n of size s and returns a 32 bit int
int signext(int n, unsigned s)
{
  unsigned shftd = n >> (s-1);
  unsigned lead_num = shftd & 1;
  unsigned r = createMask(lead_num, s);

  return r | n;


  return 0;
}

int main()
{
  printf( "signext(0x8f, 8) == 0x%1$08x \n", signext(0x8f,8));
  printf( "signext(0x18f, 12) == 0x%1$08x \n", signext(0x18f,12));
  printf( "signext(0x8f, 9) == 0x%1$08x \n", signext(0x8f,9));
  printf( "signext(0xabcd, 16) == 0x%1$08x \n", signext(0xabcd,16));

  int c;
  printf( " enter something to exit:");
  scanf("%d", &c);
  return 0;
}
