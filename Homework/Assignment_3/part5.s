.set noreorder
.data

.text
.globl main
.ent main
main:
# elt *head;
# elt *newelt;
# newelt = (elt *) MALLOC(sizeof (elt));
ori $v0, $0, 9 # syscall does MALLOC
ori $a0, $0, 8 # size of MALLOC
syscall #do the MALLOC
add $s0, $0, $v0 #s0 = newelt type elt*

# newelt->value = 1;
addi $t1, $0, 1 #t1 = 1
sw $t1, 0($s0)

# newelt->next = 0;
sw $0, 4($s0)

# head = newelt;
add $s1, $0, $s0 # s1 = head

# newelt = (elt *) MALLOC(sizeof (elt));
ori $v0, $0, 9 # syscall does MALLOC
ori $a0, $0, 8 # size of MALLOC
syscall #do the MALLOC
add $s0, $0, $v0 #s0 = newelt type elt*

# newelt->value = 2;
addi $t1, $0, 2 #t1 = 2
sw $t1, 0($s0)

# newelt->next = head;
add $t1, $0, $s1
sw $t1, 4($s0)

# head = newelt;
add $s1, $0, $s0

lw $t4, 0($s1) #t4 = head->value

# PRINT_HEX_DEC(head->value);
ori $v0, $0, 20 #sets code to print hex
add $a0, $0, $t4 #set integer to print
syscall#PRINT_HEX_DEC(A[i]);


lw $t3, 4($s1) # t3 = head -> next
lw $t4, 0($t3) # t4 = head -> next -> value
#PRINT_HEX_DEC(head->next->value);
ori $v0, $0, 20 #sets code to print hex
add $a0, $0, $t4 #set integer to print
syscall#PRINT_HEX_DEC(A[i]);

exit:
    ori $v0, $0, 10     # exit
    syscall
.end main

#typedef struct elt {
# int value;
# struct elt *next;
#} elt;
