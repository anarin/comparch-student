  .set noreorder
  .data
#char A[] = { 1, 25, 7, 9, -1 };
A: .byte 1, 25, 7, 9, -1

.text
.globl main
.ent main
#int main()
#{
main:
    #int i;
    #int i = 0;
    add $s0, $0, $0# $s0 = i = 0;
    #current = A[0];
    la $t3, A # $t3 = A
    lb $s1, 0($t3) # s1 = current = A[0];


    ##int max;

    #max = 0;
    addi $s2, $0, 0# $s2 = max = 0;

     #while (current > 0) {
    Loop: slt $t1, $s1, $0 # if current < 0
          bne $t1, $0, print # print if current < 0
          nop
          #if (current > max)
          slt $t2, $s1, $s2 # if current < max
          bne $t2, $0, increment_i
          nop
          #max = current;
          add $s2, $0, $s1 # max = current


          #i = i + 1;
          #current = A[i];
          increment_i: addi $s0, $s0, 1 # i += 1
          add $t4, $t3, $s0
          lb $s1, 0($t4) # current = A[i]


          j Loop
          nop
    print:
          ori $v0, $0, 20 #sets code to print hex
          add $a0, $0, $s2 #set integer to print
          syscall#PRINT_HEX_DEC(current);

           ori $v0, $0, 10     # exit
           syscall
            # EXIT;
            # }
.end main
