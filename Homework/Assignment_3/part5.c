#include <stdio.h>
#include <stdlib.h>

typedef struct elt {
 int value;
 struct elt *next;
} elt;
int main()
{
 elt *head;
 elt *newelt;
 newelt = (elt *) malloc(sizeof (elt));
 newelt->value = 1;
 newelt->next = 0;
 head = newelt;
 newelt = (elt *) malloc(sizeof (elt));
 newelt->value = 2;
 newelt->next = head;
 head = newelt;

 int one, two;
 one = head -> value;
 two = head -> next -> value;

 printf("head -> value 0x%1$x  (%1$d)\n", one);
 printf("head -> next -> value 0x%1$x  (%1$d)\n", two);

 int i;
 scanf("%d", &i);
}
