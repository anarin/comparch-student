.set noreorder
.data
#typedef struct record {
# int field0;
# int field1;
#} record;
record: .word 12, 0, 0 # size, field0, field1
.text
.globl main
.ent main
main:
    # record *r = (record *) MALLOC(sizeof(record));
    ori $v0, $0, 9 # syscall does MALLOC
    ori $a0, $0, 8 # size of MALLOC
    syscall #do the MALLOC
    add $s0, $0, $v0 #get the position of record

    # r->field0 = 100;
    addi $t1, $0, 100 # t1 = 100
    sw  $t1, 0($s0) # r -> field0 = 100

    addi $t2, $0, -1 #t2 = -1
    sw $t2, 4($s0) # r -> field1 = -1
exit:
    ori $v0, $0, 10     # exit
    syscall
.end main


#int main()
#{

# r->field0 = 100;
# r->field1 = -1;
# EXIT;
#}
