.set noreorder
.data

.text
.globl main
.ent main
main:
      # int main()
      # {
      # int score = 84;
      addi $s0, $0, 84 # score = s0 = 84
      # int grade;
      # if (score >= 90)
      slti $t0, $s0, 90 # t0 = 0 if s0 > 90
      bne $t0, $0, else80 # jump to else80 if above returns 1
      # grade = 4;
      addi $s1, $0, 4 # stores grad in $s1
      j print #jumps to print statement if grade is determined
      # else if (score >= 80)
      else80: slti $t0, $s0, 80 # t0 = 0 if s0 > 80
          bne $t0, $0, else70 # jump to else80 if above returns 1
          # grade = 3;
          addi $s1, $0, 3 # stores grade in $s1
          j print #jump to print if grade found
          # else if (score >= 70)
          else70: slti $t0, $s0, 70 # t0 = 0 if s0 > 70
          bne $t0, $0, else #jump to else if above returns 1
          # grade = 2;
          addi $s1, $0, 2 # stores grade in $s1
          j print # jump to print if grade found
          # else
          # grade = 0;
          else: ori $s1, $0, 0 #sets grade equal to 0
          # PRINT_HEX_DEC(grade);
          print: ori $v0, $0, 20 #sets code to print hex
            or $a0, $0, $s1 #set integer to print
            syscall

            ori $v0, $0, 10     # exit
            syscall
            # EXIT;
            # }
        .end main
