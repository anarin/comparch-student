#include <stdio.h>
#include <stdlib.h>
int A;
int B = 0;
int array1[1024];
void func1() {
int func1_0, func1_1, func1_2;
//Print locations of variables decalared in function
printf("7) Memory locations of func1_0, func1_1 and func1_2\n");
printf("func1_0 --> %p\n",&func1_0);
printf("func1_1 --> %p\n",&func1_1);
printf("func1_2 --> %p\n",&func1_2);
printf("\n");
printf("------------------------------------------");
printf("\n");
}
void func2(int func2_0, int func2_1, int func2_2) {
//Print memory locations for variables passed to function as parameters
printf("8) Memory locations of func2_0, func2_1 and func2_2\n");
printf("func2_0 --> %p\n",&func2_0);
printf("func2_1 --> %p\n",&func2_1);
printf("func2_2 --> %p\n",&func2_2);
printf("\n");
printf("------------------------------------------");
printf("\n");
}
int main() {
char C;
int D = 0;
char array2[1024];
long array3[1024];
short array4[1024];
func1();
int E, F, G;
func2(E, F, G);
char *allocated = (char *)malloc(1024);

//Print memory locations of globals A,B, and array1
printf("1) Memory locations of A, B and array1\n");
printf("A --> %p\n",&A);
printf("B --> %p\n",&B);
printf("array1 --> %p\n",array1);
printf("\n");
printf("------------------------------------------");
printf("\n");
//size of array 2
printf("2) Size of array1\n");
printf("sizeof(array1) --> %d\n", sizeof(array1));
printf("\n");
printf("------------------------------------------");
printf("\n");

//Print memory locations of main vars C, D, and array2
printf("3) Memory locations of C, D and array2\n");
printf("C --> %p\n",&C);
printf("D --> %p\n",&D);
printf("array2 --> %p\n",array2);
printf("\n");
printf("------------------------------------------");
printf("\n");
//Print size of array2
printf("4) Size of array2\n");
printf("sizeof(array2) --> %d", sizeof(array2));
printf("\n");
printf("------------------------------------------");
printf("\n");
//Print size of array3
printf("5) Size of array3\n");
printf("sizeof(array3) --> %d\n", sizeof(array3));
printf("\n");
printf("------------------------------------------");
printf("\n");
//Print size of array4
printf("6) Size of array4\n");
printf("sizeof(array4) --> %d\n", sizeof(array4));
printf("\n");
printf("------------------------------------------");
printf("\n");
//Print location of allocated
printf("10) Memory location of allocated\n");
printf("Allocated --> %p\n", allocated);
printf("\n");
printf("------------------------------------------");
printf("\n");
//Print the location of the functions called by the program
printf("11) Functions called by the program\n");
printf("func1 --> %p\n", func1);
printf("func2 --> %p\n", func2);
printf("\n");
printf("------------------------------------------");
printf("\n");


int test;
scanf("%d",&test);

}
