//This computes the same thing as the assembly program above
#include <stdio.h>
#include <stdlib.h>

int logicfcn(int in)
{
  int a = in >> 4;
  return a & 255;
}
int main()
{
  int a = 43981;
  int res = logicfcn(a);
  printf("Unsigned result: %u\n", res);
  printf("Hex result: %x\n", res);

  int test;
  scanf("%d",&test);
}
