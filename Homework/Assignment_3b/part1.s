.set noreorder
.data
#int A[8]
A: .word 8
   .space 32
.text
.globl print
.ent print
print:
  ori $v0, $0, 20
  syscall
  nop
  jr $ra
  nop
.end print
.globl main
.ent main
main:
    #PUSH s0, ra (8bytes)
    addi $sp, $sp, -8
    sw $s0, 0($sp)
    sw $ra, 4($sp)
    #int i;
    add $s0, $0, $s0 # i = 0

    #A[0] = 0;
    la $t0, A #load address of A into $t0
    lw $t1, 0($t0) #load A[0] into $t1
    add $t1, $0, $0 #set $t1 to 0
    sw $t1, 0($t0) #store 0 in A[1]

    #A[1] = 1;
    lw $t1, 4($t0)
    addi $t1, $0, 1
    sw $t1, 4($t0)

    #for (i=2; i < 8, i++){
    addi $s0, $0, 2 # i=2
    loop:
          slti $t2, $s0, 8 #t1 = 1 if i < 8
          beq $t2, $0, exit #exit if i>8
          nop

          #A[i] = A[i-1] + A[i-2];
          addi $t3, $s0, -1 # t3 = i-1
          sll $t3, $t3, 2 # t3 = 4* i-1 (byte offset)
          add $t3, $t3, $t0 # t3 = &A[i-1]
          lw  $a1, 0($t3)   # a1 = A[i-1]

          addi $t3, $s0, -2 # t3 = i-2
          sll $t3, $t3, 2 # t3 = 4* i-2 (byte offset)
          add $t3, $t3, $t0 # t3 = &A[i-2]
          lw  $a2, 0($t3)   # a2 = A[i-2]

          sll $t3, $s0, 2 # t3 = 4* i(byte offset)
          add $t3, $t3, $t0 # t3 = &A[i]
          add $t4, $a1, $a2 # t4= A[i-1] + A[i-2]
          sw $t4, 0($t3)


          #print(a[i])
          add $a0, $0, $t4 #set integer to pass to funct
          jal print
          nop

          #Increment i
          addi $s0, $s0, 1

          j loop


    exit:
    #POP stuff off sp
    #lw $s0, 0($sp)
    lw $ra, 4($sp)
    add $sp, $sp, 8
    jr $ra #return (exit)
    nop
    #ori $v0, $0, 10     # exit - old
    #syscall
.end main
