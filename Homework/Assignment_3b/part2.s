.set noreorder
.data

.text
.globl sum3
.ent sum3
#int sum3( intx, inty, intz) {
sum3:
    add $v0, $a0, $a1  #v0 = x + y
    add $v0, $v0, $a2 #v0 = v1 + z = x + y + z

    jr $ra # return
    nop
    #}
.end sum3

.globl print
.ent print
#void print(x) {
print:
  ori $v0, $0, 20
  syscall #prints value in a1
  nop
  jr $ra  #return
  nop
#}
.end print

.globl polynomial
.ent polynomial
polynomial:
#int polynomial(int a, int b, int c, int d, int e)
#load e into $t0
lw $t0, 0($sp)
# int x = s0, int y = s1, int z = s2;
#PUSH s0,s1,s2,s3 ra onto the stack 20 bytes
add $sp, $sp, -20
sw  $ra, 16($sp)
sw  $s3, 12($sp)
sw  $s2, 8($sp)
sw  $s1, 4($sp)
sw  $s0, 0($sp)
# x = a << b;
sll $s0, $a0, $a1
# y = c << d;
sll $s1, $a2, $a3
# z = sum3(x, y, e);
add $a0, $0, $s0 #a0= x
add $a1, $0, $s1 #a1 = y
add $a2, $0, $t0 #a2 = e
jal sum3
nop
add $s3, $0, $v0 #s3 = z
# print(x);
jal print
nop
# print(y);
add $a0, $0, $a1 #a0 = y
jal print
nop
# print(z);
add $a0, $0, $s3 #a0 = z
jal print
nop
add $v0, $0, $s3
#POP stuff off the stack
lw  $ra, 16($sp)
lw  $s3, 12($sp)
lw  $s2, 8($sp)
lw  $s1, 4($sp)
lw  $s0, 0($sp)

addi $sp, $sp, 20

# return z

jr $ra
nop
#}

.end polynomial

.globl main
.ent main
#int main()
#{
main:
    # PUSH ra, s0, s1 space for parameters e (16 bytes)
    addi $sp, $sp, -16 # sp = sp - 16
    sw $ra, 12($sp) # 12(sp) = ra
    sw $s0, 8($sp)  # 8(sp) = s0
    sw $s1, 4($sp)  # 4(sp) = s1

    # int f = polynomial(a, 3, 4, 5, 6);
    addi $s0, $0, 2   # s0 = int a = 2;
    add $a0, $0, $s0  #a0 = a
    addi $a1, $0, 3   #a1 = 3
    addi $a2, $0, 4   #a2 = 4
    addi $a3, $0, 5   #a3 = 5
    addi $t0, $0, 6   #t0 = 6
    sw $t0, 0($sp)    # *(sp) = t0 --> e
    jal polynomial    #v0 = call polynomial
    nop

    add $s1, $v0, $0  #f = s1 = v0

    #print(a)
    add $a0, $0, $s0 #a0 = s0
    jal print        #jump print
    nop

    #print(f)
    add $a0, $0, $s1
    jal print
    nop

    #exit with stack
    lw $ra, 12($sp)
    lw $s0, 8($sp)
    addi $sp, $sp, 16
    jr $ra # return
    nop

.end main
